import React from 'react';
import styles from './Order.module.css';

const Order = (props) => {

    const ingredients = [];
    
    for(const ingredientName in props.ingredients){
        ingredients.push(
            {
                name: ingredientName, 
                amount: props.ingredients[ingredientName]
            }
        );
    }

    const ingredientsOutput = ingredients.map(ig => {
        return <span  
            style={{
                textTransform: 'capitalize', 
                display: 'inline-block', 
                margin: '0 8px', 
                border: '1px solid #ccc', 
                padding: '5px'}}
            key={ig.name}>{ig.name} ({ig.amount})</span>;
    });


    return(
        <div className={styles.Order}>
            
            <p>{ingredientsOutput}</p>
            <p>Price: <strong>{props.price.toFixed(2)} Euros</strong></p>
        </div>
    );
}


export default Order;