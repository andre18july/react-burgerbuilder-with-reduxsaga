import React from 'react';

import styles from './BuildControls.module.css';
import BuildControl from './BuildControl/BuildControl';

const controls = [
    { label: 'Salad', type: 'salad' },
    { label: 'Bacon', type: 'bacon' },
    { label: 'Cheese', type: 'cheese' },
    { label: 'Meat', type: 'meat' }
];

const buildControls = (props) => {

    return(

        <div className={styles.BuildControls}>

            <p>Price: <strong>{props.price.toFixed(2)}</strong></p>

            {controls.map((el) => (
                
                <BuildControl 
                    key={el.label} 
                    label={el.label} 
                    addIngredient={() => props.addIngredient(el.type)} 
                    removeIngredient={() => props.removeIngredient(el.type)}
                    disabled={props.disabled[el.type]}
                />
            ))}

            <button 
                disabled={!props.purchasable} 
                className={styles.OrderButton}
                onClick={props.order}
            >
            {props.isAuthenticated ? "Order Now" : "Sign In Please"}
            
            </button>
        </div>
    )
}

export default buildControls;