import React from 'react';
import styles from './Burger.module.css';
import BurgerIngredient from './BurgerIngredient/BurgerIngredient';



const burger = (props) => {

    //console.log(props.ingredients);

    let transformedIngredients = Object.keys(props.ingredients)
        .map(key => {
            return [...Array(props.ingredients[key])].map((_ , idx) => {
                return <BurgerIngredient key={key + idx} type={key}/>;
            });
        })    
        .reduce((arr, el) => {
            return arr.concat(el);
        }, []);
        

    
    //console.log(transformedIngredients);

    if(transformedIngredients.length === 0){
        transformedIngredients = <p>Please insert some ingredients...</p>;
    }


    return(
        <div className={styles.Burger}>
            <BurgerIngredient type="bread-top"/>
            {transformedIngredients}
            <BurgerIngredient type="bread-bottom"/>
        </div>
    );

};

export default burger;