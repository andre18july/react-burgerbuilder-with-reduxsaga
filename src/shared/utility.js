export const updateObject = (oldObject, updatedProperties) => {
    return {
        ...oldObject,
        ...updatedProperties
    };
};

export const checkValidity = (value, rules) => {
    const valueCheck = value.trim();
    let isValid = true;


    if(rules.required){
        isValid = valueCheck !== '' && isValid;
    }

    if(rules.minLength){
        isValid = valueCheck.length >= rules.minLength && isValid;    
    }

    if(rules.maxLength){
        isValid = valueCheck.length < rules.maxLength && isValid;    
    }

    return isValid;
};