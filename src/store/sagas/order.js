import * as actions from '../actions/index';
import axios from '../../axios-orders';
import { put } from 'redux-saga/effects';

export function* purchaseBurgerSaga(action) {
    yield put(actions.purchaseBurgerStart());
    try {
        let response = yield axios.post('orders.json?auth='+ action.token, action.orderData);
        //console.log(res);
        yield put(actions.purchaseBurgerSuccess(response.data.name, action.orderData));

    }catch(error){
        yield put(action.purchaseBurgerFail(error));
    } 
}

export function* fetchOrdersSaga(action) {
    yield put(actions.fetchOrdersStart());
    try {
        const queryParams = '?auth=' + action.token + '&orderBy="userId"&equalTo="' + action.userId + '"';
        let response = yield axios.get('/orders.json' + queryParams);
        //console.log(response);

        const fetchOrders = [];
        for(let key in response.data){
            fetchOrders.push({
                ...response.data[key],
                id: key
            });
        }

        yield put(actions.fetchOrdersSuccess(fetchOrders));

    }catch(error){
        yield put(actions.fetchOrdersFail(error));
    } 
}